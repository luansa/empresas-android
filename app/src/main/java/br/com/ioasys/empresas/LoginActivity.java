package br.com.ioasys.empresas;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.content.Intent;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import br.com.ioasys.empresas.data.model.LoginCredentials;
import br.com.ioasys.empresas.data.remote.APIService;
import br.com.ioasys.empresas.data.remote.APIUtils;
import okhttp3.Headers;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity
        extends AppCompatActivity
        implements Callback<Object> {

    private static final String TAG = "LoginActivity";

    // Referências de UI
    private EditText mEmailView;
    private EditText mPasswordView;
    private ProgressBar mProgressView;
    private ConstraintLayout mLoginFormView;

    private Toast mErrorCredentials;

    private APIService mAPIService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.login_screen);

        mEmailView     = (EditText) findViewById(R.id.editText_email);
        mPasswordView  = (EditText) findViewById(R.id.editText_password);
        mLoginFormView = (ConstraintLayout) findViewById(R.id.login_form);
        mProgressView  = (ProgressBar) findViewById(R.id.progressBar_login_progress);
        final Button mSignInButton = (Button) findViewById(R.id.button_action_enter);

        mAPIService = APIUtils.getAPIService();

        mSignInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == EditorInfo.IME_ACTION_DONE) {
                    attemptLogin();
                }
                return false;
            }
        });

        mErrorCredentials = Toast.makeText(this,
                R.string.error_login_credentials,
                Toast.LENGTH_SHORT);

    }

    @Override
    protected void onResume() {
        showProgress(false);
        super.onResume();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    /**
     * Verifica se os dados de login são válidos e invoca {@link #sendLogin} para
     * efetuar o login.
     */
    private void attemptLogin() {
        // Reseta erros
        mEmailView.setError(null);
        mPasswordView.setError(null);

        String email = mEmailView.getText().toString();
        String password = mPasswordView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Verifica se é uma senha válida, se o usuário tiver entrado com uma.
        if (!TextUtils.isEmpty(password)) {
            if (!isPasswordValid(password)) {
                mPasswordView.setError(getString(R.string.error_invalid_password));
                focusView = mPasswordView;
                cancel = true;
            }
        } else {
            mPasswordView.setError(getString(R.string.error_field_required));
            focusView = mPasswordView;
            cancel = true;
        }

        // Verifica se é um endereço de email válido
        if (TextUtils.isEmpty(email)) {
            mEmailView.setError(getString(R.string.error_field_required));
            focusView = mEmailView;
            cancel = true;
        } else if (!isEmailValid(email)) {
            mEmailView.setError(getString(R.string.error_invalid_email));
            focusView = mEmailView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            // Mostra a progress bar e oculta o formulario de login
            showProgress(true);
            sendLogin(email, password);
        }
    }

    /**
     * Efetua login com os dados informados pelo usuário com o uso da API da iOasys.
     *
     * @param email e-mail de login.
     * @param password senha do login.
     */
    public void sendLogin(String email, String password) {
        mAPIService.login(new LoginCredentials(email, password)).enqueue(this);
    }

    /**
     * Método da interface {@link retrofit2.Callback} implementada nesta classe.
     *
     * @param call
     * @param response
     */
    @Override
    public void onResponse(Call<Object> call, Response<Object> response) {
        // Caso tenha realizado o login com sucesso
        if (response.isSuccessful()) {
            if (response.code() == 200) {
                // Coleta as custom headers e as guarda.
                Headers header = response.headers();
                APIUtils.addCustomHeader(
                        "access-token",
                        header.get("access-token")
                );
                APIUtils.addCustomHeader(
                        "client",
                        header.get("client")
                );
                APIUtils.addCustomHeader(
                        "uid",
                        header.get("uid")
                );

                Intent intent = new Intent(this, HomeActivity.class);
                startActivity(intent);
            } else {
                showProgress(false);
            }
        } else {
            mErrorCredentials.show();
            showProgress(false);
        }
    }

    /**
     * Método da interface {@link retrofit2.Callback} implementada nesta classe.
     *
     * @param call
     * @param t
     */
    @Override
    public void onFailure(Call<Object> call, Throwable t) {
        //Log.d(TAG, "Nao foi possivel enviar a requisicao a API.");
        Toast.makeText(this,
                R.string.error_onfailure,
                Toast.LENGTH_LONG).show();
        showProgress(false);
    }

    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
//        return password.length() > 4;
        return true;
    }

    /**
     * Mostra a {@link android.widget.ProgressBar} e esconde o formulário de login ou vice e versa.
     *
     * @param show {@code true} se é para mostrar a {@link android.widget.ProgressBar} e esconder
     *                         o formulário, {@code false} caso contrário.
     */
    private void showProgress(final boolean show) {
        int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

        mLoginFormView.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
        mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mLoginFormView.setVisibility(show ? View.INVISIBLE : View.VISIBLE);
            }
        });

        mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
        mProgressView.animate().setDuration(shortAnimTime).alpha(
                show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            }
            });
    }

}
