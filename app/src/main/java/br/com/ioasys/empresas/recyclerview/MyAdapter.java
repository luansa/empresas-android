package br.com.ioasys.empresas.recyclerview;

import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;

import java.util.ArrayList;
import java.util.List;

import br.com.ioasys.empresas.R;
import br.com.ioasys.empresas.data.model.Enterprise;
import br.com.ioasys.empresas.glide.GlideApp;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.EnterpriseViewHolder> {

    private static final String TAG = "MyAdapter";

    private List<Enterprise> mEnterpriseList;

    private ViewGroup mParentView;

    // Listener para click em CardView.
    private OnCardViewClickListener mOnCardViewClickListener;

    public MyAdapter() {
        mEnterpriseList = new ArrayList<>();
    }

    public MyAdapter(List<Enterprise> list) {
        mEnterpriseList = new ArrayList<>();
        mEnterpriseList.addAll(list);
    }

    public void updateList (List<Enterprise> list) {
        mEnterpriseList.clear();
        mEnterpriseList.addAll(list);
    }

    public void setOnCardViewClickListener(OnCardViewClickListener listener) {
        mOnCardViewClickListener = listener;
    }

    @Override
    public int getItemCount() {
        return mEnterpriseList.size();
    }

    @Override
    public EnterpriseViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        mParentView = parent;
        View cardView = LayoutInflater.
                from(parent.getContext()).
                inflate(R.layout.enterprise_cardview, parent, false);

        return new EnterpriseViewHolder(cardView);
    }

    @Override
    public void onBindViewHolder(EnterpriseViewHolder holder, int position) {
        Enterprise enterprise = mEnterpriseList.get(position);

        ImageView img = new ImageView(mParentView.getContext());
        GlideApp.with(mParentView).load(enterprise.getPhoto())
                .fallback(R.drawable.img_e_1)
                .error(R.drawable.img_e_1)
                .into(holder.mPhoto);

        holder.mName.setText(enterprise.getEnterpriseName());
        holder.mType.setText(enterprise.getEnterpriseType().getEnterpriseTypeName());
        holder.mCountry.setText(enterprise.getCountry());
    }

    public interface OnCardViewClickListener {
        /**
         * Invocado quando o usuário pressiona em cima de uma empresa, representada por um
         * {@link android.support.v7.widget.CardView CardView}.
         *
         * @param view A {@link android.view.View} do objeto.
         * @param object Os dados que a view possui armazenado.
         */
        void onCardViewClick(View view, Object object);
    }

    protected class EnterpriseViewHolder extends RecyclerView.ViewHolder implements
            View.OnClickListener {
        private static final String TAG = "EnterpriseViewHolder";

        protected ImageView mPhoto;
        protected TextView  mName;
        protected TextView  mType;
        protected TextView  mCountry;

        public EnterpriseViewHolder(View cardView) {
            super(cardView);

            mPhoto   = (ImageView) cardView.findViewById(R.id.card_enterprise_photo);
            mName    = (TextView) cardView.findViewById(R.id.card_enterprise_name);
            mType    = (TextView) cardView.findViewById(R.id.card_enterprise_type);
            mCountry = (TextView) cardView.findViewById(R.id.card_enterprise_country);

            cardView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            if (mOnCardViewClickListener != null)
                mOnCardViewClickListener.
                        onCardViewClick(view, mEnterpriseList.get(getAdapterPosition()));
        }
    }
}
