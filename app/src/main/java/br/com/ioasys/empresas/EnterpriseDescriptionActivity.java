package br.com.ioasys.empresas;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import br.com.ioasys.empresas.data.model.Enterprise;
import br.com.ioasys.empresas.glide.GlideApp;

public class EnterpriseDescriptionActivity extends AppCompatActivity {

    private static final String TAG = "EnterpDescripActivity";

    ImageView mPhoto;
    TextView mDescription;
    Toolbar mToolbar;

    Enterprise mEnterprise;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mEnterprise = (Enterprise) getIntent().getSerializableExtra("enterprise");

        setContentView(R.layout.enterprise_screen);

        mToolbar = (Toolbar) findViewById(R.id.toolbar_enterprise);
        mPhoto   = (ImageView) findViewById(R.id.imageView_enterprise_description);
        mDescription = (TextView) findViewById(R.id.textView_enterprise_description);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(mEnterprise.getEnterpriseName());

        GlideApp.with(this)
                .load(mEnterprise.getPhoto())
                .fallback(R.drawable.img_e_1)
                .error(R.drawable.img_e_1)
                .into(mPhoto);

        mDescription.setText(mEnterprise.getDescription());
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();

        return super.onSupportNavigateUp();
    }

}
