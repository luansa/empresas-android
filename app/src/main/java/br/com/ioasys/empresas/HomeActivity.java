package br.com.ioasys.empresas;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import br.com.ioasys.empresas.data.model.Enterprise;
import br.com.ioasys.empresas.data.model.Enterprises;
import br.com.ioasys.empresas.data.remote.APIService;
import br.com.ioasys.empresas.data.remote.APIUtils;
import br.com.ioasys.empresas.recyclerview.MyAdapter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class HomeActivity extends AppCompatActivity implements
        View.OnClickListener,
        SearchView.OnCloseListener,
        SearchView.OnQueryTextListener,
        Callback<Enterprises>,
        MyAdapter.OnCardViewClickListener {

    private static final String TAG = "HomeActivity";

    // UI references.
    private TextView     mInfoInitial;
    private SearchView   mSearchView;
    private ImageView    mAppLogo;
    private RecyclerView mRecyclerView;
    private ProgressBar  mProgressBar;

    private Toast mQuitToast;
    private Toast mFailureToast;

    private APIService mAPIService;

    private MyAdapter mMyAdapter;

    private Timer mTimer;

    private boolean mQuit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.home_screen);

        mAPIService = APIUtils.getAPIService();

        if (savedInstanceState != null) {
            APIUtils.setCustomHeaders((HashMap) savedInstanceState
                    .getSerializable("custom-headers"));
        }

        mAppLogo      = (ImageView) findViewById(R.id.imageView_app_logo);
        mInfoInitial  = (TextView) findViewById(R.id.textView_info_initial);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerView_card_enterprises);
        mProgressBar  = (ProgressBar) findViewById(R.id.progressBar_search_progress);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        ((LinearLayoutManager) mRecyclerView.getLayoutManager()).
                setOrientation(LinearLayoutManager.VERTICAL);

        mMyAdapter = new MyAdapter();
        mMyAdapter.setOnCardViewClickListener(this);
        mRecyclerView.setAdapter(mMyAdapter);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);

        mSearchView = (SearchView) findViewById(R.id.action_search);
        mSearchView.setOnSearchClickListener(this);
        mSearchView.setOnCloseListener(this);

        mSearchView.setOnQueryTextListener(this);

        mQuitToast =  Toast.makeText(this,
                R.string.information_press_again,
                Toast.LENGTH_SHORT);
        mFailureToast = Toast.makeText(this,R.string.error_onfailure,Toast.LENGTH_LONG);

        setSearchViewDetails();

        mTimer = new Timer();
    }

    @Override
    protected void onRestart() {
        super.onRestart();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putSerializable("custom-headers", APIUtils.getCustomHeaders());

        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onDestroy() {
        APIUtils.resetCustomHeaders();
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (!mQuit) {
            mQuit = true;
            mQuitToast.show();
            mTimer.schedule(new TimerTask() {
                @Override
                public void run() {
                    mQuit = false;
                }
            }, 2000);
        } else {
            mQuitToast.cancel();
            mFailureToast.cancel();
            super.onBackPressed();
        }
    }

    /**
     * Configura alguns determinados parâmetros internos da {@link android.widget.SearchView}.
     */
    public void setSearchViewDetails() {
        // Recupera o TextView que faz parte da Search View
        int id = mSearchView.getResources().getIdentifier(
                "search_src_text", "id", "android");
        AutoCompleteTextView searchTextView = (AutoCompleteTextView) findViewById(id);
        // Recupera o imageview do 'close button' para trocar por outra imagem
        id = mSearchView.getResources().getIdentifier(
                "search_close_btn", "id", "android");
        ImageView searchCloseBtn = (ImageView) findViewById(id);
        searchCloseBtn.setImageDrawable(ContextCompat.getDrawable(this,R.drawable.ic_clear_white));

        // Adicionando o icone de pesquisa no canto esquerdo da search view
        searchTextView.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_search_white,0,0,0);
        searchTextView.setCompoundDrawablePadding(10);
        // Configurando o Hint
        searchTextView.setHint(R.string.search_hint);
        // Configurando as cores
        searchTextView.setHintTextColor(ContextCompat.getColor(this,R.color.color_hint_search_view));
        searchTextView.setTextColor(ContextCompat.getColor(this,R.color.color_text_search_view));
        // Configurando a letra
        searchTextView.setTextSize(17);

    }

    /**
     * Preenche a {@link android.support.v7.widget.RecyclerView} presente no layout da Activity com
     * todas as empresas retornadas pela pesquisa feita através da API da iOasys.
     *
     * @param enterprises Classe que encapsula várias empresas.
     */
    private void fillRecyclerView(Enterprises enterprises) {
        List<Enterprise> list = enterprises.getEnterprises();
        Collections.sort(list);
        mMyAdapter.updateList(list);
        mRecyclerView.getAdapter().notifyDataSetChanged();
    }

    @Override
    public void onClick(View view) {
        if (view == mSearchView) {
            mAppLogo.setVisibility( View.GONE );

            if (mRecyclerView.getVisibility() == View.GONE)
                mInfoInitial.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onClose() {
        mAppLogo.setVisibility( View.VISIBLE );

        if (mRecyclerView.getVisibility() == View.GONE)
            mInfoInitial.setVisibility(View.VISIBLE);

        return false;
    }

    @Override
    public void onCardViewClick(View view, Object object) {
        Enterprise enterprise = (Enterprise) object;
        Intent intent = new Intent(this,EnterpriseDescriptionActivity.class);
        intent.putExtra("enterprise",enterprise);
        startActivity(intent);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        mProgressBar.setVisibility(View.VISIBLE);
        mAPIService.getEnterpriseByName(APIUtils.getCustomHeaders(), s).enqueue(this);
        if (mRecyclerView.getVisibility() == View.GONE)
            mRecyclerView.setVisibility(View.VISIBLE);

        return false;
    }

    @Override
    public boolean onQueryTextChange(String s) {
        return false;
    }

    // Quando a pesquisa feita retorna resultados.

    /**
     * Método da interface {@link retrofit2.Callback} implementada nesta classe.
     * Invocado quando for realizada uma pesquisa e esta retorna resultados.
     *
     * @param call
     * @param response
     */
    @Override
    public void onResponse(Call<Enterprises> call, Response<Enterprises> response) {
        // Quando o codigo da resposta está ok.
        if (response.isSuccessful()) {
            if (response.code() == 200) {
                fillRecyclerView(response.body());
                mProgressBar.setVisibility(View.GONE);
                mRecyclerView.requestFocus();
                mSearchView.clearFocus();
            }
        } else {
            mProgressBar.setVisibility(View.GONE);
            Toast.makeText(this,R.string.error_onresponse,Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Método da interface {@link retrofit2.Callback} implementada nesta classe.
     * Invocado quando uma pesquisa não consegue ser efetuada.
     *
     * @param call
     * @param t
     */
    @Override
    public void onFailure(Call<Enterprises> call, Throwable t) {
        mProgressBar.setVisibility(View.GONE);
        mFailureToast.show();
    }
}

