package br.com.ioasys.empresas.data.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class EnterpriseType implements Serializable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("enterprise_type_name")
    @Expose
    private String enterpriseTypeName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEnterpriseTypeName() {
        return enterpriseTypeName;
    }

    public void setEnterpriseTypeName(String enterpriseTypeName) {
        this.enterpriseTypeName = enterpriseTypeName;
    }

    @Override
    public String toString() {
        return "EnterpriseType{" +
                "id=" + id +
                ", enterpriseTypeName='" + enterpriseTypeName + '\'' +
                '}';
    }
}