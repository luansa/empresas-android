package br.com.ioasys.empresas.data.remote;


import java.util.HashMap;
import java.util.List;

import br.com.ioasys.empresas.data.model.Enterprises;
import br.com.ioasys.empresas.data.model.LoginCredentials;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIService {

    @POST("users/auth/sign_in")
    @Headers("Content-Type: application/json")
    Call<Object> login(@Body LoginCredentials credLogin);

    @GET("enterprises")
    @Headers("Content-Type: application/json")
    Call<Enterprises> getEnterpriseByName(
            @HeaderMap     HashMap<String, String> headers,
            @Query("name") String name);

}
