package br.com.ioasys.empresas.data.remote;

import android.util.Log;

import java.io.Serializable;
import java.util.HashMap;

public class APIUtils implements Serializable {

    private static final String TAG = "APIUtils";

    // URL base para o acesso à API.
    public static final String BASE_URL = "http://54.94.179.135:8090/api/v1/";

    private static APIService mAPIService = null;

    private static HashMap<String,String> mCustomHeaders = null;

    public APIUtils() {}

    public static APIService getAPIService() {
        if (mAPIService == null) {
            mAPIService = RetrofitClient.getClient(BASE_URL).create(APIService.class);
            //return mAPIService;
        }// else
        return mAPIService;
    }

    public static HashMap<String,String> getCustomHeaders() {
        return mCustomHeaders;
    }

    public static void setCustomHeaders(HashMap customHeaders) {
        mCustomHeaders = customHeaders;
    }

    public static void addCustomHeader(String h1, String h2) {
        if (mCustomHeaders == null) {
            mCustomHeaders = new HashMap<>();
        }
        mCustomHeaders.put(h1, h2);
    }

    public static void resetCustomHeaders() {
        if (mCustomHeaders != null)
            mCustomHeaders.clear();

        mCustomHeaders = null;
    }
}
